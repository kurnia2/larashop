<?php

Route::group([
    'middleware' => 'web',
    'prefix' => config('shop.admin.url') . '/products',
    'namespace' => 'LaraShop\Products\Http\Controllers'
], function()
{
    Route::get('/', 'ProductsController@index');
});
