@extends('layouts.app')

@section('content')
	<div class="row">

		<div class="col-sm-6">
			<div class="card">
				<div class="card-header">
					<strong>Dados do PagSeguro</strong>
				</div>
				<div class="card-block">
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label for="pagseguro_email">E-mail</label>
								<input type="email" class="form-control" id="pagseguro_email" name="pagseguro_email">
							</div>
						</div>
					</div>
					<!--/.row-->

					<div class="row">
						<div class="col-sm-12">
							<div class="form-group" style="margin-bottom: 0;">
								<label for="pagseguro_token">Token</label>
								<input type="text" class="form-control" id="pagseguro_token">
							</div>
						</div>
					</div>
					<!--/.row-->

				</div>
			</div>

		</div>
		<!--/.col-->

		<div class="col-sm-6">
			<div class="card">
				<div class="card-header">
					<strong>SandBox</strong>
					<small>(Testes)</small>
				</div>
				<div class="card-block">

					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label for="pagseguro_token">Token</label>
								<div class="radio">
									<label class="radio-inline" for="pagseguro_sandbox_true">
										<input type="radio" id="pagseguro_sandbox_true" name="pagseguro_sandbox" value="1"> Ativado
									</label>
									<label class="radio-inline" for="pagseguro_sandbox_false">
										<input type="radio" id="pagseguro_sandbox_false" name="pagseguro_sandbox" value="0"> Desativado
									</label>
								</div>
							</div>
						</div>
					</div>
					<!--/.row-->

					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label for="pagseguro_email">E-mail</label>
								<input type="email" class="form-control" id="pagseguro_email" name="pagseguro_email">
							</div>
						</div>
					</div>
					<!--/.row-->

					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label for="pagseguro_token">Token</label>
								<input type="text" class="form-control" id="pagseguro_token">
							</div>
						</div>
					</div>
					<!--/.row-->
				</div>
			</div>

		</div>
		<!--/.col-->
	</div>
	<!--/.row-->
@stop
