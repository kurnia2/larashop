<?php

namespace LaraShop\Settings\Entities;

use Illuminate\Database\Eloquent\Model;

class Description extends Model
{
    protected $fillable = [];
    public $timestamp = false;
    protected $table = 'settings_description';

    public function setting()
    {
    	return $this->hasOne( Setting::class );
    }
}
