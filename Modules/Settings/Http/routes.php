<?php

Route::group([
    'middleware' => 'web',
    'prefix' => config('shop.admin.url') . '/settings',
    'namespace' => 'LaraShop\Settings\Http\Controllers'
], function()
{
//    Route::get('/', 'SettingsController@index');

    Route::get('/{area}', 'SettingsController@index');
});
