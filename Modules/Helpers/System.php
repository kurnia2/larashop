<?php
namespace LaraShop\Helpers;


use LaraShop\Settings\Entities\Setting;

class System
{
    /**
     * Url do painel Admin
     * @param null $url
     * @return string
     */
    static public function url_admin($url = null )
    {
        $admin = config('shop.admin.url');
        $url = str_replace('.', '/', $url);

        return url( $admin ) . '/' . $url;
    }

    /**
     * Configuração desejada a partir da {$key}
     * @param $key
     * @param string $default
     * @return null|string
     */
    static public function config( $key, $default = '' )
    {
        $exp = explode( '.', $key );
        $settings = Setting::where( 'subarea', $exp[0] )
            ->where( 'key', $exp[1] )
            ->first();
        $config = config( 'shop.' . $key );

        if( $settings ) {
            return $settings->value;
        } elseif ( $config ) {
            return $config;
        } else {
            return ( isset( $default ) ? $default : null );
        }
    }
}