<?php

namespace LaraShop\Users\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UsersDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $user = new \LaraShop\Users\Entities\User;
        $user->name = 'LaraShop Admin';
        $user->email = 'raank92@gmail.com';
        $user->password = bcrypt( 'neerd6487' );
        $user->role = 'admin';
        $user->remember_token = str_random(16);
        $user->save();

        // $this->call("OthersTableSeeder");
    }
}
