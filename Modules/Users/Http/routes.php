<?php

Route::group([
    'middleware' => 'web',
    'prefix' => config('shop.admin.url') . '/users',
    'namespace' => 'LaraShop\Users\Http\Controllers'
], function()
{
    Route::get('/', 'UsersController@index');
});
