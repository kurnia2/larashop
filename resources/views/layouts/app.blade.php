<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Icons -->
    <link href="{{ asset('components/fontawesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('components/simple-line-icons/css/simple-line-icons.css') }}"
          rel="stylesheet">

    <!-- Main styles for this application -->
    <link href="{{ asset('dist/css/style.min.css') }}" rel="stylesheet">
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
    @include('partials.header')

    <div class="app-body">
        @include('partials.aside')
        <!-- Main content -->
        <main class="main">
            @include('partials.subheader')

            <div class="container-fluid">
                <div class="animated fadeIn">
                    @yield('content')
                </div>
            </div>
            <!-- /.conainer-fluid -->
        </main>
        @include('partials.assistent')
    </div>

    @include('partials.footer')
    @include('partials.scripts')
    @stack('scripts')
</body>
</html>
