<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link" href="{{ System::url_admin() }}"><i class="icon-speedometer"></i> Dashboard <span class="badge badge-info">NEW</span></a>
            </li>

            <li class="nav-title">
                Navegação
            </li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#"><i class="icon-puzzle"></i> Produtos</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ System::url_admin('products') }}"><i class="icon-puzzle"></i> Todos os Produtos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ System::url_admin('products.new') }}"><i class="icon-puzzle"></i> Criar Produto</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ System::url_admin('products.categories') }}"><i class="icon-puzzle"></i> Categorias</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ System::url_admin('products.tags') }}"><i class="icon-puzzle"></i> Tags</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#"><i class="icon-puzzle"></i> Páginas</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ System::url_admin('pages') }}"><i class="icon-puzzle"></i> Todas as Páginas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ System::url_admin('pages.new') }}"><i class="icon-puzzle"></i> Criar Página</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ System::url_admin('messages') }}"><i class="icon-puzzle"></i> Mensagens <span class="badge badge-info">NEW</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ System::url_admin('support') }}"><i class="icon-puzzle"></i> Suporte</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ System::url_admin('shopping') }}"><i class="icon-pie-chart"></i> Compras</a>
            </li>
            <li class="divider"></li>
            <li class="nav-title">
                Extras
            </li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#"><i class="icon-star"></i> Configurações</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ System::url_admin('settings') }}" target="_top"><i class="icon-puzzle"></i> Geral</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ System::url_admin('settings.system') }}" target="_top"><i class="icon-puzzle"></i> Sistema</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ System::url_admin('settings.layout') }}" target="_top"><i class="icon-puzzle"></i> Layout</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ System::url_admin('settings.shop') }}" target="_top"><i class="icon-puzzle"></i> Loja Virtual</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ System::url_admin('settings.pagseguro') }}" target="_top"><i class="icon-puzzle"></i> PagSeguro</a>
                    </li>
                </ul>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{ System::url_admin('users') }}"><i class="icon-user"></i> Usuários</a>
            </li>

        </ul>
    </nav>
</div>