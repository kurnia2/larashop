<header class="app-header navbar">
    <button class="navbar-toggler mobile-sidebar-toggler d-lg-none" type="button">☰</button>
    <a class="navbar-brand" href="#"></a>
    <ul class="nav navbar-nav d-md-down-none">
        <li class="nav-item">
            <a class="nav-link navbar-toggler sidebar-toggler" href="#">☰</a>
        </li>

        <li class="nav-item px-3">
            <a class="nav-link" href="{{ System::url_admin() }}">Dashboard</a>
        </li>
        <li class="nav-item px-3">
            <a class="nav-link" href="{{ System::url_admin('users') }}">Users</a>
        </li>
        <li class="nav-item px-3">
            <a class="nav-link" href="{{ System::url_admin('settings') }}">Settings</a>
        </li>
    </ul>
    <ul class="nav navbar-nav ml-auto">
        <li class="nav-item d-md-down-none">
            <a class="nav-link" href="{{ System::url_admin('notifications') }}"><i class="icon-bell"></i><span class="badge badge-pill badge-danger">5</span></a>
        </li>
        {{--<li class="nav-item d-md-down-none">--}}
            {{--<a class="nav-link" href="#"><i class="icon-list"></i></a>--}}
        {{--</li>--}}
        {{--<li class="nav-item d-md-down-none">--}}
            {{--<a class="nav-link" href="#"><i class="icon-location-pin"></i></a>--}}
        {{--</li>--}}
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <img src="img/avatars/6.jpg" class="img-avatar" alt="{{ Auth::user()->name }}">
                <span class="d-md-down-none">{{ Auth::user()->name }}</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right">

                <div class="dropdown-header text-center">
                    <strong>Minha Conta</strong>
                </div>

                <a class="dropdown-item" href="{{ System::url_admin('payments') }}"><i class="fa fa-usd"></i> Compras<span class="badge badge-success">42</span></a>
                <a class="dropdown-item" href="{{ System::url_admin('messages') }}"><i class="fa fa-envelope-o"></i> Mensagens<span class="badge badge-info">42</span></a>
                <a class="dropdown-item" href="{{ System::url_admin('support') }}"><i class="fa fa-tasks"></i> Suporte<span class="badge badge-danger">42</span></a>

                {{--<div class="dropdown-header text-center">--}}
                {{--<strong>Configurações</strong>--}}
                {{--</div>--}}

                <a class="dropdown-item" href="{{ System::url_admin('profile') }}"><i class="fa fa-user"></i> Perfil</a>
                <a class="dropdown-item" href="{{ System::url_admin('settings') }}"><i class="fa fa-wrench"></i> Configurações</a>
                <div class="divider"></div>
                <a class="dropdown-item" href="{{ url('logout') }}"><i class="fa fa-lock"></i> Sair</a>
            </div>
        </li>
        <li class="nav-item d-md-down-none">
            <a class="nav-link navbar-toggler aside-menu-toggler" href="#">☰</a>
        </li>

    </ul>
</header>