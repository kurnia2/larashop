
    <!-- Bootstrap and necessary plugins -->
    <script src="{{ asset('components/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('components/tether/dist/js/tether.min.js') }}"></script>
    <script src="{{ asset('components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('components/pace/pace.min.js') }}"></script>
    <script src="{{ asset('components/chart.js/dist/Chart.min.js') }}"></script>
    <script src="{{ asset('dist/js/views/main.min.js') }}"></script>
    <script src="{{ asset('dist/js/main.min.js') }}"></script>