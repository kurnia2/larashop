<?php

namespace LaraShop\Http\Controllers\Auth;

use Illuminate\Http\Request;
use LaraShop\Http\Controllers\Controller;

class AuthController extends Controller
{
    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function loginRedirect()
    {
        if( \Auth::check() ) {
            return redirect( config('shop.admin.url', 'admin') );
        } else {
            return redirect( url() );
        }
    }
}
